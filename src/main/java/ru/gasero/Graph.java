package ru.gasero;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

//  Этот класс представляет ориентированный граф с использованием списка смежности
class Graph {

    private static final Logger LOG = LoggerFactory.getLogger(Graph.class);

    private final Map<String, Set<String>> graph; // Список смежности

    // Конструктор
    Graph() {
        graph = new HashMap<>();
    }

    // Функция для добавления ребра в граф
    void addEdge(String key, Set<String> values) {
        LOG.debug("Adding edge to graph: " + key + " " + String.join(",", values));
        graph.put(key, values);
    }

    // Функция для поиска топологической сортировки.
    // Рекурсивно использует topologicalSortUtil()
    Stack<String> topologicalSort() {
        LOG.debug(String.format("Starting topological sorting for %d elements", graph.size()));
        Stack<String> stack = new Stack<>();

        // Помечаем все вершины как непосещенные
        Map<String, Boolean> visited = new HashMap<>();
        graph.keySet().forEach(key -> visited.put(key, false));

        // Вызываем рекурсивную вспомогательную функцию
        // для поиска топологической сортировки для каждой вершины
        visited.forEach((key, value) -> {
            if (!value) {
                topologicalSortDfs(key, graph.get(key), visited, stack);
            }
        });
        return stack;
    }

    // Рекурсивная функция, используемая topologicalSort
    void topologicalSortDfs(String key, Set<String> value, Map<String, Boolean> visited,
                            Stack<String> stack) {
        //  Помечаем текущий узел как посещенный
        LOG.debug("Processing element " + key);
        visited.put(key, true);

        // Рекурсивно вызываем функцию для всех смежных вершин
        value.forEach(
                val -> {
                    if (visited.get(val) != null && !visited.get(val)) {
                        LOG.debug("Processing subelement " + val);
                        topologicalSortDfs(val, graph.get(val), visited, stack);
                    }
                }
        );

        // Добавляем текущую вершину в стек с результатом
        stack.push(key);
    }
}