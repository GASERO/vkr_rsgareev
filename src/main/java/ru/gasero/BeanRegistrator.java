package ru.gasero;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;
import ru.gasero.metric.services.LoadMetricsService;

import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

@Component
public class BeanRegistrator {

    private static final Logger LOG = LoggerFactory.getLogger(BeanRegistrator.class);

    private final ApplicationContext context;
    private Timer timer;
    private final Graph graph = new Graph();
    private final Map<String, BeanDefinition> beanDefinitions;
    private final LoadMetricsService loadMetricsService;

    public BeanRegistrator(ApplicationContext context, LoadMetricsService loadMetricsService) throws Exception {
        this.loadMetricsService = loadMetricsService;
        LOG.info("Creating bean registrator");
        this.context = context;
        ConfigurableListableBeanFactory beanFactory = ((ConfigurableApplicationContext) context).getBeanFactory();
        Map<String, BeanDefinition> map = getBeanDefinitionMap(context);
        beanDefinitions = getSingletonLazyBeans(map);
        beanDefinitions.forEach(
                (key, value) -> graph.addEdge(key, new HashSet<>(Arrays.asList(beanFactory.getDependenciesForBean(key))))
        );
        runBeanInitializationTask(context);
        LOG.info("Successfully created bean registrator");
    }

    private Map<String, BeanDefinition> getBeanDefinitionMap(ApplicationContext context) throws NoSuchFieldException, IllegalAccessException {
        LOG.debug("Getting bean definition map from bean factory");
        Field beanDefinitionMap = DefaultListableBeanFactory.class.getDeclaredField("beanDefinitionMap");
        beanDefinitionMap.setAccessible(true);
        Map<String, BeanDefinition> stringBeanDefinitionMap = (Map<String, BeanDefinition>) beanDefinitionMap.get(context.getAutowireCapableBeanFactory());
        LOG.debug("Successfully got bean definition map from bean factory. Size: " + stringBeanDefinitionMap.size());
        return stringBeanDefinitionMap;
    }

    private void runBeanInitializationTask(ApplicationContext context) {
        LOG.debug("Creating bean initialization task");
        timer = new Timer();
        Stack<String> stack = graph.topologicalSort();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (loadMetricsService.isAllOk()) {
                    String beanName = stack.pop();
                    LOG.debug("Creating bean " + beanName);
                    context.getBean(beanName);
                    LOG.debug("Successfully creating bean " + beanName);
                    removeFromMap(beanName);
                }
            }
        }, 1000, 1000);
        LOG.debug("Successfully created bean initialization task");
    }

    private ConcurrentMap<String, BeanDefinition> getSingletonLazyBeans(Map<String, BeanDefinition> map) {
        LOG.debug("Getting lazy beans");
        ConcurrentMap<String, BeanDefinition> result = map.entrySet()
                .stream()
                .filter(entry -> {
                    try {
                        String beanClassName = entry.getValue().getBeanClassName();
                        if (beanClassName != null) {
                            return "singleton".equals(entry.getValue().getScope()) &&
                                    entry.getValue().isLazyInit();
                        }
                        return false;
                    } catch (Exception e) {
                        return false;
                    }
                })
                .collect(Collectors.toConcurrentMap(Map.Entry::getKey, Map.Entry::getValue));
        LOG.debug(String.format("Successfully got %d lazy beans", result.size()));
        return result;
    }

    public synchronized void removeFromMap(String beanName) {
        if (beanDefinitions.remove(beanName) != null) {
            LOG.debug("Removed bean from queue " + beanName);
            if (beanDefinitions.isEmpty()) {
                LOG.debug("Terminating bean registrator");
                terminate();
            }
        }
    }

    public void terminate() {
        LOG.info("Terminating bean registrator");
        timer.cancel();
        timer.purge();
        context.getAutowireCapableBeanFactory().destroyBean("BeanRegistrator");
        LOG.info("Successfully terminated bean registrator");
    }
}
