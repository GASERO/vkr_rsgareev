package ru.gasero;

import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConstructorArgumentValues;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.lang.reflect.Field;
import java.util.Map;


@SpringBootApplication
public class Application {

    public static void main(String... args) throws Exception {
        ConfigurableApplicationContext run = SpringApplication.run(Application.class);
        System.out.println();
//        run.getBean("beanRegistrator");
//        Field beanDefinitionMap = DefaultListableBeanFactory.class.getDeclaredField("beanDefinitionMap");
//        beanDefinitionMap.setAccessible(true);
//        Map<String, BeanDefinition> stringBeanDefinitionMap = (Map<String, BeanDefinition>) beanDefinitionMap.get(run.getAutowireCapableBeanFactory());
//        BeanDefinition bd = stringBeanDefinitionMap.get("beanRegistrator");
//        PropertyValues pvs = bd.getPropertyValues();
//        String[] dependsOn = bd.getDependsOn();
//        String[] beanRegistrators = run.getBeanFactory().getDependenciesForBean("beanRegistrator");
//        ConstructorArgumentValues constructorArgumentValues = bd.getConstructorArgumentValues();
//        System.out.printf("");
    }
}
