package ru.gasero;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Lazy(false)
public class LazyInitBeanPostProcessor implements BeanPostProcessor {

    private static final Logger LOG = LoggerFactory.getLogger(LazyInitBeanPostProcessor.class);

    private final BeanRegistrator beanRegistrator;

    public LazyInitBeanPostProcessor(BeanRegistrator beanRegistrator) {
        this.beanRegistrator = beanRegistrator;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        LOG.debug("Postprocessing bean " + beanName);
        beanRegistrator.removeFromMap(beanName);
        return bean;
    }
}
