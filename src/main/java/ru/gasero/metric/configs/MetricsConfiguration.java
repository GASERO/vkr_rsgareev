package ru.gasero.metric.configs;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Configuration
@ConfigurationProperties(prefix = "app.metrics")
@Setter
@Getter
public class MetricsConfiguration {
    private String rootPath;
    private String postgresBinPath;

    private Double diskThreshold;
    private Double threadThreshold;
    private Double heapThreshold;
    private Double tomcatThreadsThreshold;

    private Double systemLoadThreshold;
    private Long requestsThreshold60s;
    private Long requestsTimeThreshold;
    private Double postgresLatencyThreshold;

}
