package ru.gasero.metric.services;


public interface LoadMetricsService {
    Double getDiskUsage();

    Double getHeapMemoryUsage();

    Double getSystemLoadAverage();

    Double getTomcatThreadUsage();

    boolean isDiskUsageOk();

    Double getThreadUsage();

    boolean isThreadUsageOk();

    boolean isHeapMemoryOk();

    boolean isSystemLoadOk();

    boolean isRequestsCountOk();

    boolean isRequestsAverageTimeOk();

    boolean isTomcatThreadsOk();

    boolean isDataSourceOk();

    boolean isPostgresLatencyOk();

    boolean isAllOk();
}
