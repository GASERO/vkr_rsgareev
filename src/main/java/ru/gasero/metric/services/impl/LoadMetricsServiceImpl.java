package ru.gasero.metric.services.impl;

import org.springframework.stereotype.Service;
import ru.gasero.metric.components.DataSourceMeter;
import ru.gasero.metric.components.HttpRequestsMeter;
import ru.gasero.metric.components.PostgresMeter;
import ru.gasero.metric.components.TomcatMeter;
import ru.gasero.metric.configs.MetricsConfiguration;
import ru.gasero.metric.services.LoadMetricsService;

import java.io.File;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.OperatingSystemMXBean;
import java.lang.management.ThreadMXBean;

@Service
public class LoadMetricsServiceImpl implements LoadMetricsService {

    private final MetricsConfiguration metricsConfiguration;

    private final HttpRequestsMeter requestsMeter;
    private final TomcatMeter tomcatMeter;
    private final DataSourceMeter dataSourceMeter;
    private final PostgresMeter postgresMeter;

    public LoadMetricsServiceImpl(MetricsConfiguration metricsConfiguration,
                                  HttpRequestsMeter requestsMeter,
                                  TomcatMeter tomcatMeter,
                                  DataSourceMeter dataSourceMeter,
                                  PostgresMeter postgresMeter) {
        this.metricsConfiguration = metricsConfiguration;
        this.requestsMeter = requestsMeter;
        this.tomcatMeter = tomcatMeter;
        this.dataSourceMeter = dataSourceMeter;
        this.postgresMeter = postgresMeter;
    }

    @Override
    public Double getDiskUsage() {
        File root = new File(metricsConfiguration.getRootPath());
        return root.getUsableSpace() * 1.0 / root.getTotalSpace();
    }

    @Override
    public Double getHeapMemoryUsage() {
        MemoryMXBean memoryMXBean = ManagementFactory.getMemoryMXBean();
        return memoryMXBean.getHeapMemoryUsage().getUsed() * 1.0 / memoryMXBean.getHeapMemoryUsage().getMax();
    }

    @Override
    public Double getThreadUsage() {
        ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
        return threadMXBean.getThreadCount() * 1.0 / threadMXBean.getTotalStartedThreadCount();
    }

    @Override
    public boolean isThreadUsageOk() {
        return metricsConfiguration.getThreadThreshold() > (getThreadUsage() * 100);
    }


    @Override
    public Double getSystemLoadAverage() {
        OperatingSystemMXBean systemMXBean = ManagementFactory.getOperatingSystemMXBean();
        return systemMXBean.getSystemLoadAverage();
    }

    @Override
    public Double getTomcatThreadUsage() {
        return tomcatMeter.getBusyThreadsCount() / tomcatMeter.getAllThreadsCount();
    }

    @Override
    public boolean isDiskUsageOk() {
        return metricsConfiguration.getDiskThreshold() > (getDiskUsage() * 100);
    }

    @Override
    public boolean isHeapMemoryOk() {
        return metricsConfiguration.getHeapThreshold() > (getHeapMemoryUsage() * 100);
    }

    @Override
    public boolean isSystemLoadOk() {
        return metricsConfiguration.getSystemLoadThreshold() > getSystemLoadAverage();
    }

    @Override
    public boolean isRequestsCountOk() {
        return metricsConfiguration.getRequestsThreshold60s() > requestsMeter.getRequestsCountLastMinutes(1);
    }

    @Override
    public boolean isRequestsAverageTimeOk() {
        return metricsConfiguration.getRequestsTimeThreshold() > requestsMeter.getRequestsAverageTimeMillis();
    }

    @Override
    public boolean isTomcatThreadsOk() {
        return metricsConfiguration.getTomcatThreadsThreshold() > getTomcatThreadUsage();
    }

    @Override
    public boolean isDataSourceOk() {
        return dataSourceMeter.getDataSourceStatus().equals("UP");
    }

    @Override
    public boolean isPostgresLatencyOk() {
        return postgresMeter.getLatencyAverage() < metricsConfiguration.getPostgresLatencyThreshold();
    }

    @Override
    public boolean isAllOk() {
        return isDiskUsageOk()
                && isHeapMemoryOk()
                && isSystemLoadOk()
                && isRequestsCountOk()
                && isThreadUsageOk()
                && isTomcatThreadsOk()
                && isRequestsAverageTimeOk()
                && isRequestsAverageTimeOk()
                && isDataSourceOk()
                && isPostgresLatencyOk();
    }

}
