package ru.gasero.metric.components;

import io.micrometer.core.instrument.MeterRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ru.gasero.BeanRegistrator;


@Component
public class TomcatMeter {

    private static final Logger LOG = LoggerFactory.getLogger(TomcatMeter.class);

    private final MeterRegistry meterRegistry;

    public TomcatMeter(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }

    public Double getAllThreadsCount() {
        double value = meterRegistry.get("tomcat.threads.config.max").gauge().value();
        LOG.debug("Total tomcat thread count " + value);
        return value;
    }

    public Double getBusyThreadsCount() {
        double value = meterRegistry.get("tomcat.threads.busy").gauge().value();
        LOG.debug("Busy tomcat thread count " + value);
        return value;
    }
}
