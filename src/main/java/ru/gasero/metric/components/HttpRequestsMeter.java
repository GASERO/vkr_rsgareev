package ru.gasero.metric.components;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ru.gasero.BeanRegistrator;

import java.time.LocalDateTime;
import java.util.Deque;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicLong;


@Component
public class HttpRequestsMeter {

    private static final Logger LOG = LoggerFactory.getLogger(HttpRequestsMeter.class);

    private final AtomicLong totalRequestsCount = new AtomicLong(0);
    private final AtomicLong totalRequestsTime = new AtomicLong(0);
    private Deque<LocalDateTime> requestsTime = new ConcurrentLinkedDeque<>();

    public void incrementRequestsCount() {
        LocalDateTime now = LocalDateTime.now();
        LOG.debug("Add new request " + now);
        requestsTime.addFirst(now);
        if (requestsTime.size() > 10000) {
            LOG.debug("Queue is full, removing last request");
            LocalDateTime last = requestsTime.pollLast();
            LOG.debug("Last is request was" + last);
        }
    }

    public Long getRequestsCountLastMinutes(long minutes) {
        assert minutes >= 0;
        return getRequestsCountLastSeconds(60 * minutes);
    }

    public double getRequestsAverageTimeMillis() {
        if (totalRequestsCount.get() == 0) {
            LOG.debug("Total request count is 0");
            return 0;
        }
        double average = totalRequestsTime.get() * 1.0 / totalRequestsCount.get();
        LOG.debug("Average request time is " + average);
        return average;
    }

    private Long getRequestsCountLastSeconds(long seconds) {
        assert seconds >= 0;
        LocalDateTime before = LocalDateTime.now().minusSeconds(seconds);
        long count = 0;
        for (LocalDateTime time : new CopyOnWriteArrayList<>(requestsTime)) {
            if (time.isBefore(before)) {
                break;
            }
            count++;
        }
        LOG.debug(String.format("Last %d sec were %d requests", seconds, count));
        return count;
    }

    public void reset() {
        requestsTime = new ConcurrentLinkedDeque<>();
        totalRequestsCount.set(0);
        totalRequestsTime.set(0);
    }

    public void saveRequestDuration(long duration) {
        totalRequestsCount.incrementAndGet();
        totalRequestsTime.addAndGet(duration);
        incrementRequestsCount();
    }
}
