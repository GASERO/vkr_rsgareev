package ru.gasero.metric.components;

import com.google.common.base.Joiner;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.gasero.BeanRegistrator;
import ru.gasero.metric.configs.MetricsConfiguration;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;


@Component
public class PostgresMeter {

    private static final Logger LOG = LoggerFactory.getLogger(PostgresMeter.class);

    private final MetricsConfiguration metricsConfiguration;

    @Value("${spring.datasource.username}")
    private String username;

    @Value("${spring.datasource.password}")
    private String password;

    public PostgresMeter(MetricsConfiguration metricsConfiguration) {
        this.metricsConfiguration = metricsConfiguration;
    }

    public Double getLatencyAverage() {
        Map<String, String> results = getBenchResults();
        String value = results.get("latency average").split(" ")[0];
        LOG.debug("Average latency is " + value);
        return Double.valueOf(value);
    }

    private Map<String, String> getBenchResults() {
        Map<String, String> result = new HashMap<>();
        runBench()
                .lines()
                .forEach(line -> {
                    String delimiter;
                    if (line.contains(":")) {
                        delimiter = ":";
                    } else if (line.contains("=")) {
                        delimiter = "=";
                    } else {
                        return;
                    }
                    String[] s = line.split(delimiter);
                    result.put(s[0].trim(), s[1].trim());
                });
        LOG.debug("Bench result" + Joiner.on(",").withKeyValueSeparator("=").join(result));
        return result;
    }

    @SneakyThrows
    private BufferedReader runBench() {
        String command = String.format("./pgbench -U %s -c 10 -n", username);

        ProcessBuilder builder = new ProcessBuilder(command.split(" "));

        builder.directory(new File(metricsConfiguration.getPostgresBinPath()));
        builder.environment().put("PGPASSWORD", password);
        Process process = builder.start();

        return new BufferedReader(new InputStreamReader(process.getInputStream()));
    }
}
