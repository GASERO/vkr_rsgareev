package ru.gasero.metric.components;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.actuate.jdbc.DataSourceHealthIndicator;
import org.springframework.stereotype.Component;
import ru.gasero.BeanRegistrator;


@Component
public class DataSourceMeter {

    private static final Logger LOG = LoggerFactory.getLogger(DataSourceMeter.class);

    private final DataSourceHealthIndicator healthIndicator;

    public DataSourceMeter(DataSourceHealthIndicator healthIndicator) {
        this.healthIndicator = healthIndicator;
    }

    public String getDataSourceStatus() {
        String code = healthIndicator.health().getStatus().getCode();
        LOG.debug("Data source status code: " + code);
        return code;
    }
}
