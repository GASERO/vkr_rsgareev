package ru.gasero.metric.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import ru.gasero.BeanRegistrator;
import ru.gasero.metric.components.HttpRequestsMeter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Component
public class RequestsMetricFilter extends OncePerRequestFilter {

    private static final Logger LOG = LoggerFactory.getLogger(RequestsMetricFilter.class);

    private final HttpRequestsMeter requestsMeter;

    public RequestsMetricFilter(HttpRequestsMeter requestsMeter) {
        this.requestsMeter = requestsMeter;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        long start = System.currentTimeMillis();
        filterChain.doFilter(httpServletRequest, httpServletResponse);
        long duration = System.currentTimeMillis() - start;
        LOG.debug("Request duration " + duration);
        requestsMeter.saveRequestDuration(duration);
    }
}
