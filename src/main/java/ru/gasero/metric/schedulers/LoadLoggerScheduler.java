package ru.gasero.metric.schedulers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.gasero.BeanRegistrator;
import ru.gasero.metric.components.HttpRequestsMeter;
import ru.gasero.metric.services.LoadMetricsService;


@EnableScheduling
@Component
@RequiredArgsConstructor
public class LoadLoggerScheduler {

    private static final Logger LOG = LoggerFactory.getLogger(LoadLoggerScheduler.class);

    private final LoadMetricsService loadMetricsService;
    private final HttpRequestsMeter requestsMeter;

    @Scheduled(cron = "* * * * * ?")
    public void logLoadMetrics() {
        LOG.info("----------------------------------------");
        LOG.info("Disk usage : {}%", loadMetricsService.getDiskUsage() * 100);
        LOG.info("Heap usage : {}%", loadMetricsService.getHeapMemoryUsage() * 100);
        LOG.info("Thread usage : {}%", loadMetricsService.getThreadUsage() * 100);
        LOG.info("Tomcat thread usage : {}%", loadMetricsService.getTomcatThreadUsage());
        LOG.info("System load average : {}", loadMetricsService.getSystemLoadAverage());
        LOG.info("Request execution time average : {}", requestsMeter.getRequestsAverageTimeMillis());
        LOG.info("Request last minute count : {}", requestsMeter.getRequestsCountLastMinutes(1));
        LOG.info("----------------------------------------");
    }

}
